resource "aws_sns_topic" "hello-world-DEVOPS-PAGERDUTY" {
  name = "hello-world-DEVOPS-${var.env}"
  display_name = "hello-world-DEVOPS-${var.env}"
}

resource "aws_sns_topic_subscription" "hello-world-DEVOPS-PAGERDUTY" {
  topic_arn = "${aws_sns_topic.hello-world-DEVOPS-PAGERDUTY.arn}"
  protocol  = "https"
  endpoint  = "${var.URL_hello-world-DEVOPS-PAGERDUTY}"
  confirmation_timeout_in_minutes = 10
  endpoint_auto_confirms = true
}

resource "aws_sns_topic" "hello-world-DEVOPS-PAGERDUTY-EMAIL" {
  name = "hello-world-DEVOPS-EMAIL-${var.env}"
  display_name = "hello-world-DEVOPS-EMAIL-${var.env}"

  #Issue an api call for email subscription since not supported in terraform
  provisioner "local-exec" {
    command = "aws sns subscribe --topic-arn ${self.arn} --protocol email --notification-endpoint ${var.email_hello-world-DEVOPS-PAGERDUTY}"
  }
}
