resource "aws_security_group" "sg_app_security" {
  name        = "hello-world-app-${var.env}-AppSecurityGroup"
  description = "Application security group"
  vpc_id      = "${var.vpc_id}"

  tags {
    "Purpose" = "app server"
    "Application" = "hello-world"
  }
}


resource "aws_security_group_rule" "nodejs" {
  type        = "ingress"
  description = "NodeJS application port"
  from_port   = "3000"
  to_port     = "3000"
  protocol    = "tcp"
  self = true
  security_group_id = "${aws_security_group.sg_app_security.id}"
}


resource "aws_security_group_rule" "shared_services" {
  type        = "ingress"
  description = "OC V3 Shared Services"
  from_port   = "4118"
  to_port     = "4118"
  protocol    = "tcp"
  cidr_blocks = ["10.244.96.0/19"]
  security_group_id = "${aws_security_group.sg_app_security.id}"
}


resource "aws_security_group_rule" "nessus" {
  type        = "ingress"
  description = "Nessus Scanners"
  from_port   = "0"
  to_port     = "0"
  protocol    = "-1"
  cidr_blocks = ["10.244.100.0/24"]
  security_group_id = "${aws_security_group.sg_app_security.id}"
}


resource "aws_security_group_rule" "jumpserver" {
  type        = "ingress"
  description = "Jump Server"
  from_port   = "22"
  to_port     = "22"
  protocol    = "tcp"
  source_security_group_id = "${aws_security_group.sg_jump_security.id}"
  security_group_id = "${aws_security_group.sg_app_security.id}"
}


resource "aws_security_group_rule" "egress_app" {
  type        = "egress"
  description = "Allow all egress"
  from_port   = "0"
  to_port     = "0"
  protocol    = "-1"
  cidr_blocks = ["0.0.0.0/0"]
  security_group_id = "${aws_security_group.sg_app_security.id}"
}


resource "aws_security_group" "sg_jump_security" {
  name        = "hello-world-app-${var.env}-JumpSecurityGroup"
  description = "Jump Server security group"
  vpc_id      = "${var.vpc_id}"

  tags {
    "Application" = "hello-world"
    "Purpose" = "jump server"
  }
}


resource "aws_security_group_rule" "egress_jump" {
  type        = "egress"
  description = "Allow all egress"
  from_port   = "0"
  to_port     = "0"
  protocol    = "-1"
  cidr_blocks = ["0.0.0.0/0"]
  security_group_id = "${aws_security_group.sg_jump_security.id}"
}


resource "aws_security_group_rule" "allow_app_access_to_db" {
  description  = "Allow app instances to connect to db"
  type            = "ingress"
  from_port       = 3306
  to_port         = 3306
  protocol        = "tcp"
  source_security_group_id = "${aws_security_group.sg_app_security.id}"
  security_group_id = "${var.sg_database_id}"
}


resource "aws_security_group_rule" "allow_jump_access_to_db" {
  description  = "Allow jump server to connect to db"
  type            = "ingress"
  from_port       = 3306
  to_port         = 3306
  protocol        = "tcp"
  source_security_group_id = "${aws_security_group.sg_jump_security.id}"
  security_group_id = "${var.sg_database_id}"
}

resource "aws_security_group" "environment_tools" {
  name        = "Environment Tools"
  description = "Environment Tools"
  vpc_id      = "${var.vpc_id}"

  tags {
    "Application" = "hello-world"
    "Purpose" = "Environment Tools"
  }
}

resource "aws_security_group_rule" "allow_all" {
  type        = "ingress"
  description = "Allow all ingress"
  from_port   = "0"
  to_port     = "0"
  protocol    = "-1"
  cidr_blocks = ["10.128.0.0/16","10.129.0.0/16","10.244.96.0/19"]
  security_group_id = "${aws_security_group.environment_tools.id}"
}

resource "aws_elb" "elb" {
  name = "hello-world-app-${lower(var.env)}-app-elb"
  subnets = ["${var.app_instance_subnet_ids}"]
  security_groups = ["${aws_security_group.sg_app_security.id}"]

  access_logs {
    bucket = "${var.elb_logging_bucket}"
    bucket_prefix = "elb/${lower(var.env)}"
    interval = 60
  }

  listener {
    instance_port = 3000
    instance_protocol = "http"
    lb_port = 80
    lb_protocol = "http"
  }

  health_check {
    healthy_threshold = 3
    unhealthy_threshold = 5
    timeout = 5
    target = "HTTP:3000${var.healthcheck_url}"
    interval = 30
  }

  cross_zone_load_balancing = true
  idle_timeout = 60
  connection_draining = true
  connection_draining_timeout = 30
  internal = true

  tags = {
    "Creator" = "Bill Schwartz"
    "Application" = "HelloWorld"
    "Environment" = "${var.env}"
    "Role" = "app server"
    "Name" = "${var.env}AppElb"
  }
}


resource "aws_lb_cookie_stickiness_policy" "SocketIO" {
  name = "SocketIO"
  load_balancer = "${aws_elb.elb.id}"
  lb_port = 80
}


resource "aws_launch_configuration" "launch_config" {
  name_prefix = "hello-world-app-${var.env}-"
  image_id = "${var.ami_id}"
  instance_type = "${var.app_instance_type}"
  iam_instance_profile = "${var.app_instance_iam_profile}"
  key_name = "${var.ssh_key_name}"
  security_groups = ["${aws_security_group.sg_app_security.id}","${aws_security_group.environment_tools.id}"]
  # security_groups = ["${aws_security_group.sg_app_security.id}"]
  user_data = "${var.user_data}"
  lifecycle { create_before_destroy = true }
}


resource "aws_autoscaling_group" "data_migration" {
  depends_on = ["aws_launch_configuration.launch_config"]
  availability_zones = ["us-east-1a","us-east-1b","us-east-1c","us-east-1d","us-east-1e"]
  name = "hello-world-app-${var.env}-data-migration"
  max_size = "1"
  min_size = "1"
  desired_capacity = "1"
  health_check_type = "ELB"
  launch_configuration = "${aws_launch_configuration.launch_config.name}"
  load_balancers = ["${aws_elb.elb.id}"]
  enabled_metrics = ["GroupTerminatingInstances", "GroupInServiceInstances", "GroupMaxSize", "GroupTotalInstances", "GroupMinSize", "GroupPendingInstances", "GroupDesiredCapacity", "GroupStandbyInstances"]
  wait_for_elb_capacity = "1"
  vpc_zone_identifier = ["${var.app_instance_subnet_ids}"]
  lifecycle { create_before_destroy = true }

  tags = [
    {
      key = "Name"
      value = "${var.env}AppInstance"
      propagate_at_launch = true
    },
    {
      key = "deploy"
      value = "app"
      propagate_at_launch = true
    },
    {
      key = "Application"
      value = "HelloWorld"
      propagate_at_launch = true
    },
    {
      key = "Creator"
      value = "Bill Schwartz"
      propagate_at_launch = true
    },
    {
      key = "Environment"
      value = "${var.env}"
      propagate_at_launch = true
    },
    {
      key = "GoldImageVersion"
      value = "${var.gold_ami_description}"
      propagate_at_launch = true
    },
    {
      key = "Name"
      value = "${var.env}AppInstance"
      propagate_at_launch = true
    },
    {
      key = "Name"
      value = "${var.env}AppInstance"
      propagate_at_launch = true
    },
    {
      key = "Role"
      value = "app server"
      propagate_at_launch = true
    },
    {
      key = "Application"
      value = "hello-world"
      propagate_at_launch = true
    }
  ]
}


resource "aws_autoscaling_group" "asg" {
  depends_on = ["aws_launch_configuration.launch_config","aws_autoscaling_group.data_migration"]
  availability_zones = ["us-east-1a","us-east-1b","us-east-1c","us-east-1d","us-east-1e"]
  name = "${aws_launch_configuration.launch_config.name}"
  max_size = "${var.max_instances}"
  min_size = "${var.min_instances}"
  desired_capacity = "${var.desired_instances}"
  health_check_type = "ELB"
  launch_configuration = "${aws_launch_configuration.launch_config.name}"
  load_balancers = ["${aws_elb.elb.id}"]
  enabled_metrics = ["GroupTerminatingInstances", "GroupInServiceInstances", "GroupMaxSize", "GroupTotalInstances", "GroupMinSize", "GroupPendingInstances", "GroupDesiredCapacity", "GroupStandbyInstances"]
  vpc_zone_identifier = ["${var.app_instance_subnet_ids}"]
  lifecycle { create_before_destroy = true }

  tags = [
    {
      key = "Name"
      value = "${var.env}AppInstance"
      propagate_at_launch = true
    },
    {
      key = "deploy"
      value = "app"
      propagate_at_launch = true
    },
    {
      key = "Application"
      value = "HelloWorld"
      propagate_at_launch = true
    },
    {
      key = "Creator"
      value = "Bill Schwartz"
      propagate_at_launch = true
    },
    {
      key = "Environment"
      value = "${var.env}"
      propagate_at_launch = true
    },
    {
      key = "GoldImageVersion"
      value = "${var.gold_ami_description}"
      propagate_at_launch = true
    },
    {
      key = "Name"
      value = "${var.env}AppInstance"
      propagate_at_launch = true
    },
    {
      key = "Name"
      value = "${var.env}AppInstance"
      propagate_at_launch = true
    },
    {
      key = "Role"
      value = "app server"
      propagate_at_launch = true
    },
    {
      key = "Application"
      value = "hello-world"
      propagate_at_launch = true
    }
  ]
}


resource "aws_autoscaling_policy" "percent_capacity" {
  name = "percent_capacity"
  scaling_adjustment = "${var.percent_capacity_increase}"
  adjustment_type = "PercentChangeInCapacity"
  cooldown = 300
  autoscaling_group_name = "${aws_autoscaling_group.asg.name}"
}


resource "aws_autoscaling_policy" "target_cpu" {
  name = "target_CPU"
  autoscaling_group_name = "${aws_autoscaling_group.asg.name}"
  policy_type = "TargetTrackingScaling"

  target_tracking_configuration {
    predefined_metric_specification {
      predefined_metric_type = "ASGAverageCPUUtilization"
    }
    target_value = 80.0
  }
}


resource "random_shuffle" "public_subnets" {
  input = ["${var.jump_server_subnet_ids}"]
  result_count = 1
}


resource "aws_instance" "jump_server" {
  ami = "${var.gold_ami_id}"
  instance_type = "${var.jump_instance_type}"
  vpc_security_group_ids = ["${aws_security_group.sg_app_security.id}", "${aws_security_group.sg_jump_security.id}"]
  disable_api_termination = false
  key_name = "${var.ssh_key_name}"
  monitoring = true
  subnet_id = "${random_shuffle.public_subnets.result[0]}"
  associate_public_ip_address = true
  iam_instance_profile = "${var.app_instance_iam_profile}"

  tags {
    "Name" = "${var.env}JumpInstance_TF"
    "deploy" = "deploy"
    "Application" = "HelloWorld"
    "Creator" = "Bill Schwartz"
    "Environment" = "${var.env}"
    "GoldImageVersion" = "${var.gold_ami_description}"
    "Name" = "${var.env}JumpInstance"
    "Role" = "jumpbox"
    "Application" = "hello-world"
  }
}


resource "aws_eip" "jumpbox" {
  instance = "${aws_instance.jump_server.id}"
  vpc = true
}

resource "null_resource" "list-app-instances-script" {
  depends_on = ["aws_instance.jump_server"]
  triggers = {jumpserver_id = "${aws_instance.jump_server.id}"}

  provisioner "local-exec" {
    command = "scp -i ~/.ssh/${var.ssh_key_name}.pem ${path.cwd}/../../modules/app/list-app-instances.sh ec2-user@${aws_eip.jumpbox.public_ip}:/home/ec2-user"
  }

  provisioner "local-exec" {
    command = "ssh -i ~/.ssh/${var.ssh_key_name}.pem ec2-user@${aws_eip.jumpbox.public_ip} 'chmod +x /home/ec2-user/list-app-instances.sh'"
  }
}


resource "null_resource" "set-hostname" {
  depends_on = ["null_resource.list-app-instances-script"]
  triggers = {jumpserver_id = "${aws_instance.jump_server.id}"}

  provisioner "local-exec" {
    command = "ssh -tt -i ~/.ssh/${var.ssh_key_name}.pem ec2-user@${aws_eip.jumpbox.public_ip} 'echo \"${var.env}\" > /tmp/hostname && sudo mv /tmp/hostname /etc/hostname && sudo shutdown -r 1'"
  }
}

resource "aws_vpc_endpoint" "data_faker" {
  vpc_id            = "${var.vpc_id}"
  service_name      = "com.amazonaws.us-east-1.execute-api"
  vpc_endpoint_type = "Interface"

  security_group_ids = [
    "${aws_security_group.sg_app_security.id}"
  ]

  subnet_ids = "${var.private_subnet_ids}"
  private_dns_enabled = true

}
