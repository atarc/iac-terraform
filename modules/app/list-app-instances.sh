#!/bin/bash
# Script to list app instances in the environment

export INSTANCE_ID=$(curl http://169.254.169.254/latest/meta-data/instance-id)
export INSTANCE_TAGS="aws --region us-east-1 --output text ec2 describe-instances --instance-ids $INSTANCE_ID"
export TARGET_ENV=$($INSTANCE_TAGS | grep -i Environment | awk '{print $3}' | awk '{print tolower($0)}')
#
echo "*******************************************"
echo "App instances in $TARGET_ENV environment"
echo "*******************************************"

aws --region us-east-1 ec2 describe-instances --output text \
  --filters "Name=tag:Name,Values=*hello-world${TARGET_ENV^}AppInstance" \
  --query "Reservations[*].Instances[*].[InstanceId,PrivateIpAddress]"
