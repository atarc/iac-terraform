### Public Subnets ##############################################################

resource "aws_route_table" "public" {
    vpc_id = "${var.vpc_id}"

    route {
        cidr_block = "0.0.0.0/0"
        gateway_id = "${var.gateway_id}"
    }

    tags {
        Name = "hello-world-${var.env}-public"
    }
}


resource "aws_subnet" "public_1" {
    vpc_id = "${var.vpc_id}"
    cidr_block = "${var.public_subnet_cidr}"
    availability_zone = "us-east-1a"

    tags {
        Name = "hello-world-${var.env}-public-1"
    }
}


resource "aws_route_table_association" "public_1" {
    subnet_id = "${aws_subnet.public_1.id}"
    route_table_id = "${aws_route_table.public.id}"
}

### Private Subnets #############################################################

resource "aws_route_table" "private" {
    vpc_id = "${var.vpc_id}"

    route {
        cidr_block = "0.0.0.0/0"
        gateway_id = "${var.nat_id}"
    }

    tags {
        Name = "hello-world-${var.env}-private"
    }
}


resource "aws_subnet" "private_1" {
    vpc_id = "${var.vpc_id}"

    cidr_block = "${var.private_subnet_cidrs[0]}"
    availability_zone = "us-east-1a"

    tags {
        Name = "hello-world-${var.env}-private-1"
    }
}


resource "aws_route_table_association" "private_1" {
    subnet_id = "${aws_subnet.private_1.id}"
    route_table_id = "${aws_route_table.private.id}"
}


resource "aws_subnet" "private_2" {
    vpc_id = "${var.vpc_id}"

    cidr_block = "${var.private_subnet_cidrs[1]}"
    availability_zone = "us-east-1b"

    tags {
        Name = "hello-world-${var.env}-private-2"
    }
}


resource "aws_route_table_association" "private_2" {
    subnet_id = "${aws_subnet.private_2.id}"
    route_table_id = "${aws_route_table.private.id}"
}


resource "aws_subnet" "private_3" {
    vpc_id = "${var.vpc_id}"

    cidr_block = "${var.private_subnet_cidrs[2]}"
    availability_zone = "us-east-1c"

    tags {
        Name = "hello-world-${var.env}-private-3"
    }
}


resource "aws_route_table_association" "private_3" {
    subnet_id = "${aws_subnet.private_3.id}"
    route_table_id = "${aws_route_table.private.id}"
}
