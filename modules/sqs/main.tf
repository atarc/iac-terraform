resource "aws_sqs_queue" "deadletter" {
  name = "hello-world-${lower(var.env)}-deadletter.fifo"
  fifo_queue = true
  content_based_deduplication = false
  visibility_timeout_seconds = 30
  message_retention_seconds = 1209600
  tags = {
    Application = "hello-world"
  }
}

resource "aws_sqs_queue" "data" {
  name = "hello-world-${lower(var.env)}-data-process.fifo"
  fifo_queue = true
  content_based_deduplication = true
  visibility_timeout_seconds = 30
  message_retention_seconds = 345600
  redrive_policy = "{ \"deadLetterTargetArn\": \"${aws_sqs_queue.deadletter.arn}\", \"maxReceiveCount\": 5 }"
  receive_wait_time_seconds = 0
  tags = {
    Application = "hello-world"
  }
}

resource "aws_sqs_queue" "file-process" {
  name = "hello-world-${lower(var.env)}-file-process.fifo"
  fifo_queue = true
  content_based_deduplication = true
  visibility_timeout_seconds = 300
  message_retention_seconds = 345600
  redrive_policy = "{ \"deadLetterTargetArn\": \"${aws_sqs_queue.deadletter.arn}\", \"maxReceiveCount\": 5 }"
  receive_wait_time_seconds = 0
  tags = {
    Application = "hello-world"
  }
}
