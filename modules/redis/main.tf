resource "aws_security_group" "redis" {
  name = "hello-world-redis-${var.env}"
  description = "Redis security group"
  vpc_id = "${var.vpc_id}"

  tags {
    "Application" = "hello-world"
    "Purpose" = "redis server"
    "Application" = "hello-world"
  }
}

resource "aws_security_group_rule" "app_nodes" {
  type = "ingress"
  description = "app nodes"
  from_port = "6379"
  to_port = "6379"
  protocol = "tcp"
  source_security_group_id = "${var.app_nodes_sec_group_id}"
  security_group_id = "${aws_security_group.redis.id}"
}

resource "aws_security_group_rule" "redis_nodes" {
  type = "ingress"
  description = "redis nodes"
  from_port = "6379"
  to_port = "6379"
  protocol = "tcp"
  source_security_group_id = "${aws_security_group.redis.id}"
  security_group_id = "${aws_security_group.redis.id}"
}

resource "aws_security_group_rule" "jumpserver" {
  type = "ingress"
  description = "jump server"
  from_port = "22"
  to_port = "22"
  protocol = "tcp"
  source_security_group_id = "${var.jumpserver_sec_group_id}"
  security_group_id = "${aws_security_group.redis.id}"
}

resource "aws_security_group_rule" "egress" {
  type        = "egress"
  description = "Allow all egress"
  from_port   = "0"
  to_port     = "0"
  protocol    = "-1"
  cidr_blocks = ["0.0.0.0/0"]
  security_group_id = "${aws_security_group.redis.id}"
}

resource "aws_instance" "redis" {
  count = "${var.cluster_instance_count}"
  ami = "${var.ami_id}"
  instance_type = "${var.instance_type}"
  vpc_security_group_ids = ["${aws_security_group.redis.id}"]
  disable_api_termination = false
  key_name = "${var.ssh_key_name}"
  monitoring = true
  subnet_id = "${var.subnet_id}"
  associate_public_ip_address = true
  iam_instance_profile = "${var.app_instance_iam_profile}"

  tags {
    "Name" = "hello-world${var.env}Redis${count.index}"
    "deploy" = "deploy"
    "Application" = "hello-world"
    "Creator" = "Bill Schwartz"
    "Environment" = "${var.env}"
    "GoldImageVersion" = "${var.gold_ami_description}"
    "Name" = "hello-world${var.env}Redis"
    "Role" = "redis"
    "Application" = "hello-world"
  }
}

resource "null_resource" "wait" {
  depends_on = ["aws_instance.redis"]
  triggers = {redis_master_id = "${aws_instance.redis.0.id}"}

  provisioner "local-exec" {
    command = "sleep 120"
  }
}

resource "null_resource" "configure_redis_master" {
  depends_on = ["null_resource.wait"]
  triggers = {redis_master_id = "${aws_instance.redis.0.id}"}

  provisioner "local-exec" {
    command = "ssh -i ~/.ssh/${var.ssh_key_name}.pem ec2-user@${var.jumpserver_ip} \"ssh ${aws_instance.redis.0.private_ip} ./master.sh\" "
  }
}

resource "null_resource" "configure_redis_slaves" {
  depends_on = ["null_resource.configure_redis_master"]
  triggers = {redis_master_id = "${aws_instance.redis.0.id}"}

  provisioner "local-exec" {
    command =<<EOF
set -e;
set -x;

MASTER_IP="${aws_instance.redis.0.private_ip}";
SSH_KEYNAME="${var.ssh_key_name}.pem";
JUMPSERVER_IP="${var.jumpserver_ip}";
PASSWORD=$(ssh -i ~/.ssh/$SSH_KEYNAME ec2-user@$JUMPSERVER_IP ssh $MASTER_IP "grep pass redis.conf");
PASSWORD=$(echo $PASSWORD| awk -F' ' '{print $2}');
LAST_INDEX_NUMBER=$(expr "${var.cluster_instance_count}" - 1);

for x in $(seq 1 $LAST_INDEX_NUMBER);do
  SLAVE_IP=$(terraform state show module.redis.aws_instance.redis\[$x\] | grep private_ip | awk '{print $3}');
  ssh -i ~/.ssh/$SSH_KEYNAME ec2-user@$JUMPSERVER_IP ssh $SLAVE_IP "./slave.sh $PASSWORD $MASTER_IP";
done
EOF
  }
}
