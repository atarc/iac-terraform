module "ssh_keys" {
  source = "../../../modules/ssh_keys"
  private_key = "${var.private_key}"
  authorized_keys_file = "${path.cwd}/access/authorized_keys"
  jumpserver_ip = "${var.jumpserver_ip}"
}
