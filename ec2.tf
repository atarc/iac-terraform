# AMI

data "aws_ami" "ami-0c2b8ca1dad447f8a" {
  most_recent = true
  owners = ["137112412989"]
  filter {
    name = "name"
    values = ["amzn2-ami-hvm-2.0.20210721.2-x86_64-gp2"]
  }

  filter {
    name = "architecture"
    values = ["x86_64"]
  }

  filter {
    name = "root-device-type"
    values = ["ebs"]
  } 
}

# EC2 instance

resource "aws_instance" "web" {
  instance_type = "t2.micro"
  ami = data.aws_ami.ami-0c2b8ca1dad447f8a.id
#  key_name = aws_key_pair.gitlab.id
  key_name = "gitops-atarc"
  vpc_security_group_ids = [aws_security_group.main_sg.id]
#  user_data = "${file("scripts/webapp.sh")}"
  user_data = "${file("initapp.sh")}"

  tags = {
    Name = "gitops-atarc"
    env = var.env
  }
}

# SSH key

resource "aws_key_pair" "gitlab" {
  key_name_prefix = "gitlab-atarc"
  public_key = chomp(tls_private_key.gitlab.public_key_openssh)
}

resource "tls_private_key" "gitlab" {
  algorithm = "RSA"
  rsa_bits = "2048"
}

resource "local_file" "ec2_private_key" {
  content = tls_private_key.gitlab.private_key_pem
  filename = "${path.module}/ec2.pem"
}

# Generate dotenv artifact for GitLab

resource "local_file" "dotenv" {
  content = "EC2_EXTERNAL_IP=${aws_instance.web.public_dns}"
  filename = "${path.module}/deploy.env"
}

resource "local_file" "commit" {
  content = "COMMIT_HASH"
  filename = "${path.module}/commit.env"
}

# Generate EC2 inventory for Ansible

resource "local_file" "ansible_inventory" {
  content = aws_instance.web.public_ip
  filename = "${path.module}/hosts.ini"
}

output "env-dynamic-url" {
  value = aws_instance.web.public_dns
}

# resource "aws_route53_record" "www" {
#   zone_id = "" # Enter Zone ID
#   name    = "" # Enter domain name
#   type    = "A"
#   ttl     = "300"
#   records = [aws_instance.web.public_ip]
# }
